from distutils.core import setup
import py2exe
import sys
sys.path.append("..")
sys.path.append(r"..\twill\other_packages")

class Target:
    def __init__(self, **kw):
        self.__dict__.update(kw)
        # for the versioninfo resources
        self.version = "1.0.0"
        self.company_name = "agYeY Solutions"
        self.copyright = "gYeY Solutions"
        self.name = "JagteRaho (Keep Alive) Services"

myservice = Target(
    description = 'JagteRaho (Keep Alive) Services',
    modules = ['svc_jagteraho'],
    cmdline_style='pywin32'
)

setup(
    options = {"py2exe": {"compressed": 1, "bundle_files": 1} },    
    console=["../svc_jagteraho.py"],
    zipfile = None,
    service=[myservice],
    data_files=[("",["../jagteraho.cfg","../jagteraho.bat","../README.txt",])]
) 
